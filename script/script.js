function myFunction() {
  var x = document.getElementById("Topnav");
  if (x.className === "nav") {
    x.className += " responsive";
  } else {
    x.className = "nav";
  }
}

$(document).ready(function (){
  $(window).scroll(function () {
   var windowpos = $(window).scrollTop();
   if (windowpos >= 70) {
     $("#name").removeClass("name_before").addClass("name_after");
     $(".top_header").css({"justify-content":"space-between"})
   }
   else {
     $("#name").removeClass("name_after").addClass("name_before");
     $(".top_header").css({"justify-content":"flex-end"})
   }
 });
});

function readCSV(file) {
  var csv = require('csv');
  var fs = require('fs');
  var obj = csv();
  obj.from.path(file).to.array(function (data) {
      for (var index = 0; index < data.length; index++) {
          console.log(data[index]);
      }
  });
}
